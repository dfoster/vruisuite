// Modules to control application life and create native browser window
const {app, BrowserWindow, dialog} = require('electron')
const {ipcMain} = require('electron')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow

function createWindow () {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 880,
    height: 600,
    minWidth: 880,
    autoHideMenuBar: true
  });

  // Hide menu bar
  // mainWindow.setMenu(null);

  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  mainWindow.webContents.openDevTools()

  // Emitted when the window is closed.
  mainWindow.on('closed', function () {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null
  })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow()
  }
})

// Hide main menu while Vrui application is open
ipcMain.on('child', (event, arg) => {
  if (arg == 'yes')
    mainWindow.hide();
  
  else
    mainWindow.show();
})

// Open system file selection menu
ipcMain.on('open-file-dialog', (event) => {
  dialog.showOpenDialog({
    filters: [
      // {name: 'Images', extensions: ['jpg', 'png', 'gif']},
      // {name: 'Movies', extensions: ['mkv', 'avi', 'mp4']},
      {name: 'PLY', extensions: ['ply']}
      // {name: 'All Files', extensions: ['*']}
    ]
    // properties: ['openFile', 'openDirectory']
  }, (files) => {
    if (files) {
      // Only send one file
      event.sender.send('selected-directory', files[0])
    }
  })
})