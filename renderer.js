// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.
const { spawn } = require('child_process');
const {ipcRenderer} = require('electron')

// Global variables for spawned Vrui processes
let LidarPreprocessor;
let lidarViewer;

let fileToConvert;
let fileName;
let filePath;

document.getElementById('lidar-preprocessor').onclick = spawnLidarPreprocessor;
document.getElementById('lidar-viewer').onclick = spawnLidarViewer;
document.getElementById('select-directory').onclick = function(event) { ipcRenderer.send('open-file-dialog') };

ipcRenderer.on('selected-directory', (event, path) => {
  console.log(path);
  fileName = path.substring( path.lastIndexOf("/") + 1, path.indexOf('.') );
  filePath = path.substring(0, path.lastIndexOf("/"));

  console.log(filePath);

  document.getElementById('selected-file').innerHTML = `File: ${filePath}`
})

function spawnLidarPreprocessor () {
  LidarPreprocessor = spawn(`${__dirname}/bin/LidarPreprocessor`, ['-o', `${filePath}/${fileName}.lidar`, filePath]);

  LidarPreprocessor.stdout.on('data', function(data) {
    var str = new TextDecoder("utf-8").decode(data);
    console.log(str);
  });
  
  LidarPreprocessor.on('close', (code) => {
    if (code === 0)
      alert('Conversion complete!');
      
    console.log(`child process exited with code ${code}`);
  });
}

function spawnLidarViewer () {
  var lidarDataPath = document.getElementById('lidar-data').files[0].path;

  if (lidarDataPath.split('.')[1] == 'lidar')
    lidarViewer = spawn(`${__dirname}/bin/LidarViewer`, [lidarDataPath]);
  
  ipcRenderer.send('child', 'yes');

  lidarViewer.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
    ipcRenderer.send('child', 'no');
  });
}